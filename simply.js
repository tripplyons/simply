var data = (localStorage.getItem('data') || "").split(',');
simply.title('Vibes');
simply.subtitle(data.toString());

function save() {
    localStorage.setItem('data', data.toString());
    simply.subtitle(data.toString());
}

if((localStorage.getItem('data') || " ")[0] == ',') {
    data = [];
    save();
}

simply.on('singleClick', function(e) {
    var b = e.button;
    if(b === "up") {
        data.push('long');
    }
    if(b === "select") {
        data.push('short');
    }
    if(b === "down" && data.length !== 0) {
        data.pop();
    }
    
    save();
});
simply.on('longClick', function(e) {
    
});

var current = 0;
function vibe() {
    simply.vibe();
    if(current !== data.length-1) {
        setTimeout(function() {
            current += 1;
            vibe();
        }, (data[current] === 'short')?500:1000)
    }
}

vibe();